﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BACS_387_ISBN_AA
{
    class Check_ISBN
    {
        public string ISBN { get; set; }

        public string checkDigit(string ISBN)
        {

            int total = 0;

            for (int Z = 0; Z < 9; Z++)
                total += (10 - Z) * Int32.Parse(ISBN[Z].ToString());

            int mod = total % 11;
            int checkDigit = 11 - mod;

            if (checkDigit == 10)
                return ("X");
            else
                return (checkDigit.ToString());
        }

    }
}
