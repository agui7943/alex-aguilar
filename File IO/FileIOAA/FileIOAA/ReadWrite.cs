﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOAA
{
    class ReadWrite
    {
        public string[] readFile(Data data)
        {
            string[] Rf = File.ReadAllLines(data.file);
            return Rf;
        }
        public void WriteFile(Data data)
        {
            StreamWriter sw = new StreamWriter(data.file, true);
            if (data.read == true)
            {
                sw.WriteLine("Author: " + data.author + ",  Title: " + data.title + ", Number of Copies: " +data.copies+ ", Date Published: " + data.date + ", Read: yes");
            }
            else
            {
                sw.WriteLine("Author: " + data.author + ",  Title: " + data.title + ", Number of Copies: " + data.copies + ", Date Published: " + data.date + ", Read: no");
            }
            sw.Close();
        }
    }
}
