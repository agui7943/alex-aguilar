﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for AddChoice.xaml
    /// </summary>
    public partial class AddChoice : Window
    {
        public AddChoice()
        {
            InitializeComponent();
        }

        private void btnType_Click(object sender, RoutedEventArgs e)
        {
            AddBook bookAdd = new AddBook();
            this.Close();
            bookAdd.Show();
        }

        private void btnISBN_Click(object sender, RoutedEventArgs e)
        {
            AddByAPI apiAdd = new AddByAPI();
            this.Close();
            apiAdd.Show();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }
    }
}
