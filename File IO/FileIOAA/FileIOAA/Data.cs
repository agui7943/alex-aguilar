﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIOAA
{
    class Data
    {
        public string file;
        public string title;
        public string author;
        public int copies;
        public DateTime date;
        public bool read;
    }
}
