﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for SearchBooks.xaml
    /// </summary>
    public partial class SearchBooks : Window
    {
        public SearchBooks()
        {
            InitializeComponent();
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Books newBooks = new Books();
            Book book = new Book();
            book.title = txtSearch.Text;
            string result = newBooks.bookSearch(book);
            string[] split = result.Split(',');
            if (split[0] == "")
            {
                txtResults.Text = ("No Book Found");
            }
            else
            {
                txtResults.Text = ("Title - " + split[0] + "\nAuthor - " + split[1] + "\nISBN - " + split[2] + "\nYear - " + split[3] + "\nGenre - " + split[4] + "\nPublisher - " + split[5] + "\n");
            }
            
        }

        private void btnShowAll_Click(object sender, RoutedEventArgs e)
        {
            lstbxResults.Items.Clear();
            Books list = new Books();
            string[] results = list.bookReport();
            int i = 0;
            int len = results.Length;
            while (i < len)
            {
                lstbxResults.Items.Add(results[i]);
                i++;
            }
        }
    }
}
