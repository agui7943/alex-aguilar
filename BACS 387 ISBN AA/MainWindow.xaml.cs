﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BACS_387_ISBN_AA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalcISBN_Click(object sender, RoutedEventArgs e)
        {
            Check_ISBN num = new Check_ISBN();

            string ISBN = txtisbn.Text;
            string digit = num.checkDigit(ISBN);

            Check_Variable.Content = ("The check symbol is " + digit);
            FullISBN.Content = ("The full ISBN is " + ISBN + "-" + digit);
        }
    }
}
