﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileIOAA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnget_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.ShowDialog();
            txtFile.Text = of.FileName;
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("File Saved Correctly.");


        }

        private void btnWrite_Click(object sender, RoutedEventArgs e)
        {
            Data data = new Data();
            ReadWrite write = new ReadWrite();
            data.title = txtTitle.Text;
            data.author = txtAuthor.Text;
            data.copies = Convert.ToInt32(txtCopies.Text);
            data.date = DateTime.Parse(txtDate.Text);
            data.read = txtReadBook.Text == "yes" ? true : false;
            data.file = txtFile.Text;
            write.WriteFile(data);
            txtAuthor.Clear();
            txtCopies.Clear();
            txtDate.Clear();
            txtReadBook.Clear();
            txtTitle.Clear();

        }

        private void btnread_Click(object sender, RoutedEventArgs e)
        {
            ReadWrite rw = new ReadWrite();
            Data data = new Data();
            data.file = txtFile.Text;
            string[] result = rw.readFile(data);
            foreach (string book in result)
            {
                Books.Items.Add(book);
            }
        }
    }
}