﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GroupProjectCfAa
{
    class WebAPI
    {
        //I referred to Swaffer's code on bitbucket. 
        //Take a look at it when you have chance
        //Just talked to swaffer and he said we don't need json unless the web api uses it.
        public class MainAccess
        {
            private string BookFile = "book.json";
            public void SaveMain(Books book)
            {
                string jsonAccess = JsonConvert.SerializeObject(book);
                File.AppendAllText(BookFile, jsonAccess);

                List<Books> results = ReadAllbook();
                results.Add(book);
                string json2 = JsonConvert.SerializeObject(results);
                File.WriteAllText(BookFile, json2);
            }

            public List<Books> ReadAllbook()
            {
                if (!File.Exists(BookFile))
                {
                    return new List<Books>();
                }
                string alltext = File.ReadAllText(BookFile);
                List<Books> results = JsonConvert.DeserializeObject<List<Books>>(alltext);
                return new List<Books>();
            }
        }
    }
}
